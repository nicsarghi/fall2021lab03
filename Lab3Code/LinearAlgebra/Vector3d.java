// Nicoleta Sarghi 2039804
public class Vector3d {
    private double x;
    private double y;
    private double z;
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;        
    }
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getZ() {
        return z;
    }
    public double magnitude(){
        return(Math.sqrt(x*x+y*y+z*z));
    }
    public double dotProduct(Vector3d v2){
        return (x*v2.getX())+(y*v2.getY())+(z*v2.getZ());
    }
    public Vector3d add(Vector3d v2){
        return new Vector3d(x+v2.getX(),y+v2.getY(),z+v2.getZ());
    }
}
