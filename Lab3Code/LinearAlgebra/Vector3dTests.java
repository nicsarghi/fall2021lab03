import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
// Nicoleta Sarghi 2039804
public class Vector3dTests {
    @Test
    // Tests the get methods for the x,y and z values
    public void testGetAxisMethods(){
        Vector3d v = new Vector3d(1,7,10);
        assertEquals(1,v.getX());
        assertEquals(7,v.getY());
        assertEquals(10,v.getZ());
    }
    // Tests the magnitude method for the vector
    @Test
    public void testMagnitude(){
        final double EXPECTED_MAGNITUDE = Math.sqrt(12);
        Vector3d v = new Vector3d(2,2,2);
        assertEquals(EXPECTED_MAGNITUDE,v.magnitude());
    }
    // Tests the dotproduct method for two vectors
    @Test
    public void testDotProduct(){
        final double EXPECTED_PRODUCT = 32;
        // create two vectors to test the dot product
        // and check both of them as the dot product is commutative
        Vector3d v1 = new Vector3d(1,2,3);
        Vector3d v2 = new Vector3d(1,5,7);
        assertEquals(EXPECTED_PRODUCT,v1.dotProduct(v2));
        assertEquals(EXPECTED_PRODUCT,v2.dotProduct(v1));
    }
    // Tests the add function for vector3
    @Test
    public void testAdd(){
        // create two vectors to test the addition function
        Vector3d v1 = new Vector3d(1,2,3);
        Vector3d v2 = new Vector3d(1,5,7);
        // check v1
        assertEquals(2,v1.add(v2).getX());
        assertEquals(7,v1.add(v2).getY());
        assertEquals(10,v1.add(v2).getZ());
        // check v2
        assertEquals(2,v2.add(v1).getX());
        assertEquals(7,v2.add(v1).getY());
        assertEquals(10,v2.add(v1).getZ());
    }
}
